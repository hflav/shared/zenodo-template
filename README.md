# Zenodo template

An overview of the results can be found in [Unitarity.pdf](Unitarity.pdf). Individual averages included are
- Measurements of sin(β)=sin(φ₁) [JSON](sin2beta.json), [Compressed EPS figure](btoccsS_CP.eps.gz), [PNG figure](btoccsS_CP.png)
- Some other measurement


**************************************************************************************************************
XXX_END_OF_PREVIEW_XXX

Leave the marker *XXX_END_OF_PREVIEW_XXX* in place as everything below it will be omitted from the uploaded preview. In that way the instructions can be left in place for the file in Git. Also leave the marker XXX_ZENODO_RECORD_XXX as it is automatically replaced during the upload to ensure that the correct DOI is in place. Note that the DOI referencing will not work for a test file uploaded to the Zenodo sandbox and even when uploaded to the public Zenodo, the link can take a few days to go live pending registration in the central DOI database.

The purpose of this file is to offer a brief description of the results in the preview of the Zenodo page. As
the page is previewed in a protected environment, it is limited what you can do in the MarkDown text.
- You can use general text formatting
- You can link to local files like [Unitarity.pdf](Unitarity.pdf)
- You can use Unicode characters
- You can't embed images
- You can't include raw HTML

This file will be renamed to `aa_README.md` as Zenodo is showing a preview of the file that appears first in alphabetic order. It should be kept in mind that when a Zenodo record is published, it is imutable and the only way to fix the record is to upload a new version which will then get a new DOI. However, the information in the `.description.txt` file can be updated later. So use this field for publication information and similar that might only be known after the record is created.

There is the option to use the *Zenodo sandbox* where records are not permanent if you want to play around first without leaving any permanent record.

## Preparation
The most important part of the record is the json file(s) that encode the averages and give information about the publications that went into them. An example [sin2beta_example.json](sin2beta_example.json) is provided. There can be a single JSON file or multiple. The format of the example file should be adhered to. Extreme care should be taken to ensure that the JSON file, the figures and any other files use identical numbers. The file `.gitattributes` list files that are present in git but should not be uploaded to the `Zenodo` record. In the file `.zenodo.json` update the fields `title`, `creator` and `description`. The rest should be left unchanged.

## To-do once

1. Decide on the results that will be part of this repository. This can in principle be *all* the results of a working group, but can also be a smaller group. In general things that will always be updated together, should be kept as a single upload. A maximum of 100 files and 50GB is allowed.

1. Fork the project. In the top right of the Gitlab webpage you see the fork icon. Click it. On the following page, select `hflav` in the Project URL box. Pick
a suitable name that describes the averages in it well.Make the project `private`. Now that you are in your own repository, go to `Code -> Tags` and delete all the existing tags.

1. Create an account in [Zenodo](https://zenodo.org)

2. [Create an API token](https://zenodo.org/account/settings/applications/tokens/new/) on zenodo, 
checking deposit write and deposit publish as rights. Copy the string right away, it is shown only once. 
If you failed to do this, simply remove the token and create a new one.

3. [Create a custom variable in the Gitlab UI](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui). 
![Adding a custom variable](token-variable-screenshot.png)
    - To do this, go to your repository, then in the left bar in Settings > CI/CD. 
    - Expand the `Variables` tab.
    - Click `Add Variable`
    - Enter `zenodo_token` as the variable name and the corresponding token as a value.
    - Because this tokens allow anyone to push versions and deposits to Zenodo, you should check both "Protect" and "Mask" but not "Expand".

## Add the information
Now add the files in the repository that contains the averages and their description. Look at the exisiting formats to understand what to do. For the `.description.txt` file that contains the description of the record, the format is raw HTML. For security reasons, the following tags are the only ones accepted by the Zenodo server: a, abbr, acronym, b, blockquote, br, code, caption, div, em, i, li, ol, p, pre, span, strike, strong, sub, table, caption, tbody, thead, th, td, tr, u, ul.

## Validation
All JSON files should be validated. This is done as part of the pipeline that runs before an upload but can also be done
through a command like

    python -m json.tool sin2beta.json

## Upload

The next step is to upload the record to Zenodo. You create an upload by going to the release page in GitLab (Deploy -> Releases) and create a release with a name that is of the format `vX.Y.Z`. As soon as you create the release in GitLab, the continuous integration will perform the upload. Take a look at the `CI/CD -> Pipelines` tab in Gitlab to see if everything went OK.

Now go to [Zenodo deposit](https://zenodo.org/deposit), select your upload and you can do a `preview`. If everything is fine then click `submit for review`.

If the record doesn't upload to Zenodo, take a look at the pipeline in Gitlab (Build -> Pipelines) to see what went wrong.

If there is a problem when inspecting the record in Zenodo, you can click `discard version` instead of `submit for review` and subsequently in Gitlab delete the tag. That allows you to repeat.

**If you do not see `submit for review` but see `publish` instead, you need to add the record to the `hflav` community. See top of web page where there is a `Select a community`.**

## Publication
When advertising the page, it is important to use the link in the format `https://doi.org/10.5072/zenodo.XXX_ZENODO_RECORD_XXX` where the `XXX_ZENODO_RECORD_XXX` is replaced with the actual number from the published web page. This is the link that is promised to exist forever, even if Zenodo is changing name, CERN is closed down and the EU disappears. The record will become part of the HFLAV collection once the HFLAV conveners approve it.

## Sandbox
You can also create an account in the [Zenodo sandbox](https://sandbox.zenodo.org). While you can use Orcid authentication for your Zenodo account, the sandbox one has to be with email and password. [Create a Zenodo sandbox token](https://sandbox.zenodo.org/account/settings/applications/tokens/new/) as well. As above create a `zenodo_token_sandbox` variable name and the corresponding token as the value.

The sandbox behaves like the real Zenodo, but the records will be deleted after a while and the DOI resolver will not work for them. You create an upload by going to the release page in GitLab (Deployments -> Releases) and create a release with a name that is of the format `vX.Y.Z-preA`. The `pre` part is what directs the upload to the sandbox.

## Authors
Ulrik Egede, Monash University

## License
CC-BY-4.0
